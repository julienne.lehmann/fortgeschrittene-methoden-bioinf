import argparse
import numpy as np
import networkx as nx

parser = argparse.ArgumentParser(description="This script runs the generic Pebble Game for (k, l)-sparse graphs in 3D.")

parser.add_argument('-i', '--inputfile', type=str, default="body_bar_hinge_framework/testgraphs/graph_nicht_steif", help="Path to the coordinates of the inputgraph.")
parser.add_argument('-o', '--outputdir', type=str, default="", help="Path to the output directory.")
parser.add_argument('-l', '--l_value', type=int, default=1, help="Maximum amount of vertices between two nodes.")
parser.add_argument('-k', '--k_value', type=int, default=2, help="Maximum amount of neighobors of a node.")
args = parser.parse_args()

#pos = Knotenkoordinaten
#edges = Kantenverbindungen

def main():
    """
    main function
    """
    pos, edges = read_graph(args.inputfile, args.k_value)
    G = create_graph(edges, pos)
    #print(graph.edges())
    matrix, zeilen = create_matrix(edges, pos, G)
    leereListe = matrix_umgestalten(zeilen, matrix)
    rank = matrixrank(leereListe)

    if rank == zeilen:
        print("\nDie Matrix hat vollen Rang, der Graph ist steif.\n Der volle Rang wäre: " + str(zeilen) + "\nDer Rank der Matrix ist: " + str(rank))
    else:
        print("\nDie Matrix hat keinen vollen Rang, der Graph ist nicht steif.\n Der volle Rang wäre: " + str(zeilen) + "\nDer Rank der Matrix ist: " + str(rank))


def read_graph(inputfile, k):
    """
    Read graph from file
    :param inputfile: String specifiying the inputfile
    :param k: Maximum amount of neighobors of a node
    :return: list of all edges, the position of each node
    """
    edges = []
    pos = {}
    with open(inputfile, "r") as graphfile:
        for line in graphfile:
            line = line.rstrip()
            if ":" in line:
                pos[int(line.split(":")[0])] = (float(line.split(":")[1].split(",")[0]), float(line.split(":")[1].split(",")[1]), float(line.split(":")[1].split(",")[2]))
            else:
                edges.append((int(line.split(",")[0]), int(line.split(",")[1])))
    return pos, edges

def create_graph(edges, pos):
    """
    Create graph
    :param edges: list of egdes
    :param pos: position of every node
    :return: created graph
    """
    G = nx.Graph()
    for edge in edges:
        G.add_edge(edge[0], edge[1])

    #show_graph(G, pos, "inputgraph")
    return G

def create_matrix(edges, pos, G):
    """
    creates and fills the matrix (ridgidity-matrix)
    :param edges: list of egdes
    :param pos: position of every node
    :param G: created graph
    :return: filled matrix, number of egdes
    """
    #zeilen: Anzahl Kanten
    #spalten: Anzahl Knoten
    zeilen = len(edges)
    spalten = len(pos)
    matrix = np.empty((zeilen, spalten), dtype=object)
    print(matrix)

    counter = 0

    for kante in G.edges():
        print(kante)
        knoten1 = pos[kante[0]]
        knoten2 = pos[kante[1]]

        #counter entspricht der Zeile in die die Ergebnisse geschrieben werden
        #np.subtract berechnet die Differenz zwischen den Koordinaten und schreibt sie als Tupel in die Matrix
        matrix[counter][kante[0] - 1] = tuple(np.subtract(knoten1, knoten2))
        matrix[counter][kante[1] - 1] = tuple(np.subtract(knoten2, knoten1))
        #print(matrix)
        counter += 1

    for i in range(0,zeilen):
        for j in range(0,spalten):
            #matrix[matrix == None] = (0)
            #print(matrix)
            if matrix[i][j] is None:
                matrix[i][j] = (0.0,0.0,0.0)
    print(matrix)

    return matrix, zeilen

def matrix_umgestalten(zeilen, matrix):
    """
    Converts matrix into list
    :param zeilen: number of egdes
    :param matrix: created matrix
    :return: a list containing the rows of the matrix
    """
    leereListe = []
    listeZeile = []
    #print(leereListe)
    #counter = -1

    for zeile in matrix:
        #counter += 1
        if listeZeile != []:
            leereListe.append(listeZeile)
            listeZeile = []
        for eintrag in zeile:
            for zahl in eintrag:
                listeZeile.append(zahl)
    leereListe.append(listeZeile)
    print(leereListe)

    return leereListe

def matrixrank(leereListe):
    """
    determins the rank of the matrix
    :param leereListe: a list containing the rows of the matrix
    :return: rank of the matrix
    """
    rank = np.linalg.matrix_rank(leereListe)
    #print(rank)

    return rank

if __name__ == '__main__':
    main()
