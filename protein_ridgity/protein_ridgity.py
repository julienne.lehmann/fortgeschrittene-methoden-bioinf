import argparse
import networkx as nx
import sys
sys.path.insert(0, 'pebble_game')
from pebble_game import pebble_game_3d as pebble_game
from graphein.protein.config import ProteinGraphConfig
from graphein.protein.graphs import construct_graph
from graphein.protein.visualisation import plotly_protein_structure_graph
from graphein.protein.edges.atomic import add_atomic_edges


parser = argparse.ArgumentParser(description="This script runs the generic Pebble Game for (k, l)-sparse protein graphs in 3D.")

parser.add_argument('-i', '--inputfile', type=str, default="protein_ridgity/testgraphs/118l.pdb", help="Path to the coordinates of the inputgraph.")
parser.add_argument('-o', '--outputdir', type=str, default="", help="Path to the output directory.")
parser.add_argument('-l', '--l_value', type=int, default=6, help="Maximum amount of edges between two nodes.")
parser.add_argument('-k', '--k_value', type=int, default=6, help="Maximum amount of neighobors of a node.")
args = parser.parse_args()


def read_pdb_file(filename: str) -> tuple:
    """
    Read pdb file to graph and plot it on a atom level.
    :param filename: String specifying the path and name of the pdb inputfile
    :return: list of nodes and list of edges of the atomic graph
    """
    # Load the default config
    #c = ProteinGraphConfig(granularity='CA')
    params_to_change = {"granularity": "atom", "edge_construction_functions": [add_atomic_edges]}

    c = ProteinGraphConfig(**params_to_change)
    # Construct the graph!
    g = construct_graph(config=c, path=filename)
    # add key component
    for edge in g.edges(data=True):
        edge[-1]['component'] = 0
    

    name = filename.split("/")[-1]
    p = plotly_protein_structure_graph(
    g,
    colour_edges_by="component",
    colour_nodes_by="chain",
    label_node_ids=False,
    node_size_min=5,
    node_alpha=0.85,
    node_size_multiplier=1,
    plot_title=f"Atom-level graph (PDB: {name})."
    )

    p.show()
    return g.nodes(), g.edges()


def create_graph(edges: list) -> nx.MultiGraph:
    """
    Create networkx graph
    :param edges: list of given edges
    :param pos: dict of the position of each node
    :return: created graph
    """
    G = nx.MultiGraph()
    for edge in edges:
        G.add_edge((":".join(edge[0].split(":")[-2:])), (":".join(edge[1].split(":")[-2:])))
    print(G.edges())
    return G


def create_outfile(components: list, outputdir: str, filename: str) -> None:
    """
    Create outfile containing a list of all components with their nodes.
    :param components: list of all components with a list of their nodes per component
    :param outputdir: String specifying the outputdir to save the file to.
    :param filename: STring specifying the name of the outputfile.
    :return: None
    """
    with open(outputdir+filename, "w") as outf:
        for i in range(0, len(components)):
            outf.write(f"Component {i+1}:\n")
            for node in components[i]:
                outf.write(f"\t{node}\n")


def show_graph(components: list, filename: str) -> None:
    """
    Show the pdb file graph on a atomic level with their component nodes colored in adifferent way.
    :param components: list of all components with a list of their nodes per component
    :param filename: String specifying the path and name of the pdb inputfile
    :return: None
    """
    # add key component
    params_to_change = {"granularity": "atom", "edge_construction_functions": [add_atomic_edges]}

    c = ProteinGraphConfig(**params_to_change)
    # Construct the graph!
    g = construct_graph(config=c, path=filename)

    for edge in g.edges(data=True):
        in_component = False
        for i in range(0, len(components)):
            if ":".join(edge[0].split(":")[-2:]) in components[i] and ":".join(edge[1].split(":")[-2:]) in components[i]:
                edge[-1]['component'] = i + 1
                in_component = True
                break
        if not in_component:
            edge[-1]['component'] = 0
       

    name = filename.split("/")[-1]
    p = plotly_protein_structure_graph(
    g,
    colour_edges_by="component",
    colour_nodes_by="chain",
    label_node_ids=False,
    node_size_min=5,
    node_alpha=0.85,
    node_size_multiplier=1,
    plot_title=f"Atom-level graph with components (PDB: {name})."
    )

    p.show()


def main():
    """
    main function
    """
    nodes, edges = read_pdb_file(args.inputfile)
    graph = create_graph(edges)
    pebbles = {}
    for node in graph.nodes():
        pebbles[node] = args.k_value
    pebbles, rejected, out_graph, components = pebble_game(graph, pebbles, args.k_value, args.l_value)
    

    sum_pebbles = 0
    for vertex in pebbles:
        sum_pebbles += pebbles[vertex]
    if sum_pebbles <= args.l_value + 1:
        if len(rejected) == 0:
            print("well-constrained")
        else:
            print("over-constrained")
    else:
        if len(rejected) == 0:
            print("under-constrained")
        else:
            print("other")

    print(components)
    show_graph(components, args.inputfile)
    outfile = args.inputfile.split("/")[-1].split(".")[0]
    create_outfile(components, args.outputdir, outfile)

if __name__ == '__main__':
    main()