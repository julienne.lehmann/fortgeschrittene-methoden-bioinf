import pytest
import pebble_game.pebble_game as pg
import networkx as nx


def test_create_graph():
    edges = [(1,2), (1,3), (1,4), (3,4), (2,3)]
    pos = {1: (0, 1, 0), 2: (1, 0, 0), 3: (-1, 1, 0), 4: (0, 0, 0)}
    G = nx.MultiGraph()
    for edge in edges:
        G.add_edge(edge[0], edge[1])
    result = pg.create_graph(edges, pos)
    assert len(result.edges()) == len(G.edges())
    assert result.nodes() == G.nodes()


def test_read_graph():
    edges = [(1,2), (1,3), (1,5), (2,3), (3,4), (4,5)]
    pos = {1: (-1, 0, 0), 2: (-1, 0.3, 0), 3: (2, 0.17, -3), 4: (4, 0.255, 1), 5: (5, 0.03, 2)}
    pebbles = {1: 2, 2: 2, 3: 2, 4: 2, 5: 2}
    result = pg.read_graph("pebble_game/testgraphs/testgraph", 2)
    assert result[0] == pos
    assert result[1] == edges
    assert result[2] == pebbles


def test_transfer_pebble():
    path = [1, 3]
    edges = [(1,2), (1,3), (1,4), (3,4), (2,3)]
    G = nx.DiGraph()
    for edge in edges:
        G.add_edge(edge[0], edge[1])
    edges_new = [(1,2), (1,4), (3,4), (2,3), (3,1)]
    G_new = nx.DiGraph()
    for edge in edges_new:
        G_new.add_edge(edge[0], edge[1])
    pebbles = {1: 1, 2: 2, 3: 3, 4: 2}
    result = pg.transfer_pebble(path, pebbles, G)
    assert result[1] == pebbles
    assert result[0].edges() == G_new.edges()


def test_deep_search_True():
    G = nx.DiGraph()
    edges = [(1,2), (1,3), (1,4), (3,4), (2,3)]
    for edge in edges:
        G.add_edge(edge[0], edge[1])
    pebbles = {1: 0, 2: 2, 3: 3, 4: 2}
    G_new = nx.DiGraph()
    edges_new = [(1,3), (1,4), (3,4), (2,3), (2,1)]
    for edge in edges_new:
        G_new.add_edge(edge[0], edge[1])
    pebbles_new = {1: 1, 2: 1, 3: 3, 4: 2}
    result = pg.deep_search(G, 1, 3, pebbles)
    assert result[0].edges() == G_new.edges()
    assert result[1] == pebbles_new
    assert result[2] == True


def test_deep_search_False():
    G = nx.DiGraph()
    edges = [(1,2), (1,3), (1,4), (3,4), (2,3)]
    for edge in edges:
        G.add_edge(edge[0], edge[1])
    pebbles = {1: 0, 2: 0, 3: 0, 4: 0}
    result = pg.deep_search(G, 1, 2, pebbles)
    assert result[0].edges() == G.edges()
    assert result[1] == pebbles
    assert result[2] == False


def test_deep_search_True_complicated():
    G = nx.DiGraph()
    edges = [(1,2), (1,3), (3,4), (2,3)]
    for edge in edges:
        G.add_edge(edge[0], edge[1])
    pebbles = {1: 0, 2: 0, 3: 0, 4: 2}
    G_new = nx.DiGraph()
    edges_new = [(1,3), (2,1), (3,2), (4,3)]
    for edge in edges_new:
        G_new.add_edge(edge[0], edge[1])
    pebbles_new = {1: 1, 2: 0, 3: 0, 4: 1}
    result = pg.deep_search(G, 1, 2,pebbles)
    assert result[0].edges() == G_new.edges()
    assert result[1] == pebbles_new
    assert result[2] == True

    pebbles_new = {1: 0, 2: 0, 3: 0, 4: 1}
    G_new2 = nx.DiGraph()
    edges_new2 = [(1,3), (2,1), (3,2), (4,3)]
    for edge in edges_new2:
        G_new2.add_edge(edge[0], edge[1])
    pebbles_new2 = {1: 0, 2: 0, 3: 0, 4: 1}
    result2 = pg.deep_search(G_new, 1, 2, pebbles_new)
    assert result2[0].edges() == G_new2.edges()
    assert result2[1] == pebbles_new2
    assert result2[2] == False


def test_insert_edge():
    G = nx.DiGraph()
    pebbles = {1: 2, 2: 2, 3: 2, 4: 2}
    G_new = nx.DiGraph()
    G_new.add_edge(1,2)
    pebbles_new = {1: 1, 2: 2, 3: 2, 4: 2}
    result = pg.insert_edge(G, 1, 2, pebbles)
    assert result[0].edges() == G_new.edges()
    assert result[1] == pebbles_new


def test_reach():
    G = nx.DiGraph()
    edges = [(1,2), (1,5), (1,4), (3,4), (2,3), (6, 1)]
    for edge in edges:
        G.add_edge(edge[0], edge[1])
    result = pg.reach(1, G)
    assert result == set([2, 5, 4, 3])


def test_get_component():
    G = nx.DiGraph()
    edges = [(1,2), (1,3), (1,4), (2,3)]
    for edge in edges:
        G.add_edge(edge[0], edge[1])
    pebbles = {1: 0, 2: 0, 3: 0, 4: 2}
    result = pg.get_component(G, pebbles, 1, 2, 6)
    assert result == [1,2,3]


def test_get_component_0():
    G = nx.DiGraph()
    edges = [(1,2), (1,3), (3,4), (2,3)]
    for edge in edges:
        G.add_edge(edge[0], edge[1])
    pebbles = {1: 1, 2: 1, 3: 1, 4: 2}
    result = pg.get_component(G, pebbles, 1, 2, 6)
    assert result == 0


def test_get_component_1():
    G = nx.DiGraph()
    edges = [(1,2), (1,3), (3,4), (4,5)]
    for edge in edges:
        G.add_edge(edge[0], edge[1])
    pebbles = {1: 0, 2: 3, 3: 0, 4: 0, 5:1}
    result = pg.get_component(G, pebbles, 1, 2, 6)
    assert result == []