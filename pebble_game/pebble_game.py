import argparse
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import matplotlib.colors as mcolors

parser = argparse.ArgumentParser(description="This script runs the generic Pebble Game for (k, l)-sparse graphs in 3D.")
parser.add_argument('-i', '--inputfile', type=str, default="pebble_game/testgraphs/SR1_testgraph", help="Path to the coordinates of the inputgraph.")
parser.add_argument('-o', '--outputdir', type=str, default="", help="Path to the output directory.")
parser.add_argument('-l', '--l_value', type=int, default=6, help="Maximum amount of edges between two nodes.")
parser.add_argument('-k', '--k_value', type=int, default=6, help="Maximum amount of neighobors of a node.")
args = parser.parse_args()


def transfer_pebble(path: list, pebbles: dict, graph: nx.MultiDiGraph) -> tuple:
    """
    Transfer pebble from one node to another and reverse edges in the graph where the pebble was moved
    :param path: List of nodes
    :param pebbles: dictonairy with the amount of pebbles per node
    :param graph: current direct graph
    :return: updated direct graph, new amount of pebbles per node
    """
    pebbles[path[0]] = pebbles[path[0]] + 1
    pebbles[path[-1]] = pebbles[path[-1]] - 1
    for i in range(0, len(path)-1):
        graph.remove_edge(path[i], path[i+1])
        graph.add_edge(path[i+1], path[i])
    return graph, pebbles


def deep_search(graph: nx.MultiDiGraph, vertex: int, node2: int, pebbles: dict) -> tuple:
    """
    Deep search from specific node and get path
    :param graph: current direct graph
    :param vertex: integer specifying the vertex
    :param node2: integer specifying the second vertex
    :param pebbles: dictonairy with the amount of pebbles per node
    :return: updated directected graph, new amount of pebbles per node, boolean if the deep search was successfull
    """
    path = []
    no_direct_edge = True
    for edge in graph.edges():
        if edge[0] == vertex:
            no_direct_edge = False
            break
    if no_direct_edge: 
        return graph, pebbles, False
    dfs = nx.dfs_edges(graph, vertex)
    for edge in dfs:
        if edge[0] in path:
            while path[-1] is not edge[0]:
                path.pop()
        if edge[0] == vertex:
            path = [vertex]
        if pebbles[edge[1]] > 0 and edge[1] is not node2:
            path.append(edge[1])
            new_graph, pebbles = transfer_pebble(path, pebbles, graph)
            return new_graph, pebbles, True
        path.append(edge[1])
    return graph, pebbles, False


def insert_edge(D: nx.MultiDiGraph, node1: int, node2: int, pebbles: dict) -> tuple:
    """
    Insert new edge in the directed graph
    :param D: current directed graph
    :param node1: integer specifying the first vertex
    :param node2: integer specifying the second vertex
    :return: uodated directed graph, new amount of pebbles per node
    """
    D.add_edge(node1, node2)
    pebbles[node1] = pebbles[node1] - 1
    return D, pebbles


def reach(u: int, graph_D: nx.MultiDiGraph) -> set:
    """
    Get the nodes which can be reaches with an directed edge 
    :param u: integer specifying the vertex
    :param graph_D: current directed graph
    :return: set containg the vertices in the reach of the given node
    """
    reaches = []
    dfs = nx.dfs_edges(graph_D, u)
    for edge in dfs:
        reaches.append(edge[1])
    return set(reaches)


def create_queue(graph_D: nx.MultiDiGraph, vertex_: set) -> set:
    """
    Creates the queue with all nodes in our graph minus the nodes in the vertex set and add only those with an edge into the vertex set.
    :param graph_D: Current directed graph
    :param vertex_: Current vertex set of the component
    :return: queue = V\V' with an edge into V'
    """
    queue = set()
    for vertex in set(graph_D.nodes()).difference(vertex_):
        for edge in graph_D.edges():
            if edge[0] == vertex:
                if edge[1] in vertex_:
                    queue.add(vertex)
    return queue
    

def get_component(graph_D: nx.MultiDiGraph, pebbles: dict, u: int, v: int, l: int) -> list:
    """
    Implementation of the Component detection 1 as described in Lee at al(2007)
    :param graph_d: current directed graph
    :param pebbles: dictonairy with the amount of pebbles per node
    :param u: integer specifying the vertex u
    :param v: integer specifying the vertex v
    :param l: Maximum amount of edges between two nodes
    :return: vertex set of the component, else 0
    """
    wasqueued = set()
    reach_u = reach(u, graph_D)
    reach_v = reach(v, graph_D)
    reach_uv = (reach_u & reach_v)
    for node in reach_uv:
        if node == u or node == v: continue
        if pebbles[node] > 0:
            return 0
    vertex_ = reach_uv
    queue = create_queue(graph_D, vertex_)
    while len(queue) > 0:
        w = queue.pop()
        wasqueued.add(w)
        reach_w = reach(w, graph_D)
        no_pebbles = False
        for node in reach_w:
            if node != u and node != v:
                no_pebbles = True
                if pebbles[node] != 0:
                    no_pebbles = False
                    break
        if no_pebbles:
            vertex_ = list(set(vertex_) & reach_w)
            adding_queue = create_queue(graph_D, vertex_)
            for node in adding_queue:
                if node not in wasqueued:
                    queue.add(node)
    if len(vertex_) > 0:
        vertex_.append(u)
        vertex_.append(v)

    return list(set(vertex_))


def update_components(component: set, edge_set:set, components: set) -> list:
    """
    Search if vertices in the new component are also present in the already found components and remove them
    :param component: Set of vertices of the new component
    :param components: Set of already found components
    :return: updated list of components
    """
    result_components = []
    for comp in components:
        is_unique = True
        for edge in edge_set:
            if edge[0] in comp and edge[1] in comp:
                is_unique = False
        if is_unique:
            result_components.append(comp)
                
    result_components.append(set(component))
    return result_components


def get_edge_set(graph, component):
    comp_edges = set()
    for edge in graph.edges():
        if edge[0] in component and edge[1] in component:
            comp_edges.add(edge)
    return comp_edges


def pebble_game_3d(graph: nx.MultiDiGraph, pebbles: dict, k: int, l: int) -> tuple:
    """
    Play the pebble game on a given (k,l) sparse graph
    :param graph: input graph
    :param pebbles: amount of pebbles per edge
    :param k: Maximum amount of neighobors of a node
    :param l: Maximum amount of edges between two nodes
    :return: updated amount of pebbles per node, list of rejected edges, created directed graph, set of components
    """
    reject = []
    components = set()
    graph_D = nx.MultiDiGraph()
    graph_D.add_nodes_from(graph.nodes())
    for i in range(0, 5):
        for edge in graph.edges():
            rejected = False
            if sum(pebbles.values()) < l:
                reject.append(edge)
                continue
            for component in components:
                if (edge[0] in component and edge[1] in component) or edge[0] == edge[1]:
                    reject.append(edge)
                    rejected = True
                    break
            if rejected:
                continue
            if pebbles[edge[0]] + pebbles[edge[1]] >= l+1:
                if pebbles[edge[0]] == 0:
                    graph_D, pebbles = insert_edge(graph_D, edge[1], edge[0], pebbles)
                else:
                    graph_D, pebbles = insert_edge(graph_D, edge[0], edge[1], pebbles)
            else:
                while pebbles[edge[0]] + pebbles[edge[1]] < l+1:
                    result = False
                    result2 = False
                    graph_D, pebbles, result = deep_search(graph_D, edge[0], edge[1], pebbles)
                    if not result:
                        graph_D, pebbles, result2 = deep_search(graph_D, edge[1], edge[0], pebbles)
                    if not result and not result2:
                        break
                if pebbles[edge[0]] + pebbles[edge[1]] == l+1:
                    if pebbles[edge[0]] == 0:
                        graph_D, pebbles = insert_edge(graph_D, edge[1], edge[0], pebbles)
                    else:
                        graph_D, pebbles = insert_edge(graph_D, edge[0], edge[1], pebbles)
                else:
                    reject.append(edge)
                    continue
            if pebbles[edge[0]] + pebbles[edge[1]] > l:
                continue
            component = get_component(graph_D, pebbles, edge[0], edge[1], l)
            if component != 0:
                if len(component) > 0:
                    edge_set = get_edge_set(graph_D, component)
                    components = update_components(component, edge_set, components)
    return pebbles, reject, graph_D, components


def show_graph(G: nx.MultiGraph, pos: dict, title: str) -> None:
    """
    Plot the given graph
    :param G: networkx Graph
    :param pos: dict containing the position of each node
    :param title: Title of the output graph
    :return: None
    """
    node_xyz = np.array([pos[v] for v in sorted(G)])
    edge_xyz = np.array([(pos[u], pos[v]) for u, v in G.edges()])

    # Create the 3D figure
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")

    # Plot the nodes - alpha is scaled by "depth" automatically
    ax.scatter(*node_xyz.T, s=100, c="black")

    # Plot the edges
    for vizedge in edge_xyz:
        ax.plot(*vizedge.T, color="tab:gray")

    # Turn gridlines off
    ax.grid(False)
    # Suppress tick labels
    for dim in (ax.xaxis, ax.yaxis, ax.zaxis):
        dim.set_ticks([])
    # Set axes labels
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    ax.set_title(title)

    plt.savefig(title+".png")
    plt.show()
    return None


def show_graph_components(G: nx.MultiGraph, pos: dict, components: list, title: str) -> None:
    """
    Plot the given graph
    :param G: networkx Graph
    :param pos: dict containing the position of each node
    :param title: Title of the output graph
    :return: None
    """
    colors = list(mcolors.TABLEAU_COLORS.values())
    no_comp = set()
    edge_comps = []
    for edge in G.edges():
        in_comp = False
        for comp in components:
            edge_set = get_edge_set(G, comp)
            if edge in edge_set or (edge[1],edge[0]) in edge_set:
                in_comp = True
        if not in_comp:
            no_comp.add(edge)
    edge_xyz = np.array([(pos[u], pos[v]) for u, v in sorted(no_comp)])

    for comp in components:
        edge_set = get_edge_set(G, comp)
        edge_comps.append(np.array([(pos[u], pos[v]) for u, v in sorted(edge_set)]))
    node_xyz = np.array([pos[v] for v in G.nodes()])

    # Create the 3D figure
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    
    # Plot the nodes - alpha is scaled by "depth" automatically
    ax.scatter(*node_xyz.T, s=100, c="black")

    # Plot the edges
    for vizedge in edge_xyz:
        ax.plot(*vizedge.T, color="tab:gray")

    for i in range(0, len(edge_comps)):
        for e in edge_comps[i]:
            ax.plot(*e.T, color=colors[i])
    

    # Turn gridlines off
    ax.grid(False)
    # Suppress tick labels
    for dim in (ax.xaxis, ax.yaxis, ax.zaxis):
        dim.set_ticks([])
    # Set axes labels
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    ax.set_title(title)

    plt.savefig(title+".png")
    plt.show()
    return None


def read_graph(inputfile: str, k: int) -> tuple:
    """
    Read graph from file
    :param inputfile: String specifiying the inputfile
    :param k: Maximum amount of neighobors of a node
    :return: dict of the position of each node, list of all edges, amount of pebbles per node
    """
    edges = []
    pos = {}
    pebbles = {}
    with open(inputfile, "r") as graphfile:
        for line in graphfile:
            line = line.rstrip()
            if ":" in line:
                pos[int(line.split(":")[0])] = (float(line.split(":")[1].split(",")[0]), float(line.split(":")[1].split(",")[1]), float(line.split(":")[1].split(",")[2]))
                pebbles[int(line.split(":")[0])] = k
            else:
                edges.append((int(line.split(",")[0]), int(line.split(",")[1])))
    return pos, edges, pebbles


def create_graph(edges: list, pos: dict) -> nx.MultiGraph:
    """
    Create networkx graph
    :param edges: list of given edges
    :param pos: dict of the position of each node
    :return: created graph
    """
    G = nx.MultiGraph()
    G.add_edges_from(edges)
    print(G.edges())
    print(pos)
    show_graph(G, pos, "Inputgraph")
    return G


def main():
    """
    main function
    """
    k_value = args.k_value
    l_value = args.l_value

    pos, edges, pebbles = read_graph(args.inputfile, k_value)
    graph = create_graph(edges, pos)
    #print(graph.nodes())

    pebbles, rejected, out_graph, components = pebble_game_3d(graph, pebbles, k_value, l_value)
    
    sum_pebbles = 0
    for vertex in pebbles:
        sum_pebbles += pebbles[vertex]
    if sum_pebbles <= l_value + 1:
        if len(rejected) == 0:
            print("well-constrained")
        else:
            print("over-constrained")
    else:
        if len(rejected) == 0:
            print("under-constrained")
        else:
            print("other")

    print(out_graph.edges())
    print("Components: ")
    print(components)
    
    if len(components) == 0:
        show_graph(out_graph, pos, "Outputgraph")
    else:
        show_graph_components(graph, pos, components, "Outputgraph")
    # create output: list of nodes in the component
    print(pebbles)


if __name__ == '__main__':
    main()
