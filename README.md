# 3D rigity analysis
The programm offers multiple ways to test the rigity of a 3D graph:
1. **Body–Bar-and-Hinge Frameworks**
2. **3D Pebble Games**
3. **Protein Rigidity**

## Installation
To download the latest source from the Git server, do this:

    git clone https://codebase.helmholtz.cloud/julienne.lehmann/fortgeschrittene-methoden-bioinf.git

Go in the downloaded project and install the conda enviornment from the given environment.yaml and activate the environment.

    cd fortgeschrittene-methoden-bioinf
    conda env create -f environment.yml
    conda activate rigity_analysis
Now you can run every script within the project.

## 1. Body-Bar-and-Hinge Frameworks
Run this script to test whether a given graph is rigid. Therefore start it like this:

     python body_bar_hinge_framework/rigiditymatrix.py
There you have the  following command line options as shown in the help page:

`python body_bar_hinge_framework/rigiditymatrix.py -h`

    usage: rigiditymatrix.py [-h] [-i INPUTFILE] [-o OUTPUTDIR] [-l L_VALUE] [-k K_VALUE]
    
    This script runs the generic Pebble Game for (k, l)-sparse protein graphs in 3D.
    optional arguments: 
      -h, --help            	show this help message and exit
      -i STR, --inputfile STR	Path to the coordinates of the inputgraph
      -o STR, --outputdir STR	Path to the output directory.
	    -l INT, --l_value INT 	Maximum amount of vertices between two nodes.
	    -k INT, --k_value INT		Maximum amount of neighobors of a node.
Inputfiles have the following structure:

    [number of node]:[coordinates] for every node in the graph
    [node],[node] for every edge in the graph

Example can be shown in [graph_steif](https://codebase.helmholtz.cloud/julienne.lehmann/fortgeschrittene-methoden-bioinf/-/blob/main/body_bar_hinge_framework/graph_steif?ref_type=heads):

    1:0,0,0
    2:1,0,0
    3:0,1,0
    4:0,0,1
    5:1,1,1
    1,2
    1,3
    1,5
    2,3
    3,4
    4,5

## 2. 3D Pebble Games
Run this script to test whether a given graph has rigid components. Therefore start it like this:

    python pebble_game/pebble_game.py
There you have the  following command line options as shown in the help page:

`python pebble_game/pebble_game.py -h`

    usage: pebble_game.py [-h] [-i INPUTFILE] [-o OUTPUTDIR] [-l L_VALUE] [-k K_VALUE]
    
    This script runs the generic Pebble Game for (k, l)-sparse protein graphs in 3D.
    optional arguments: 
      -h, --help            	show this help message and exit
      -i STR, --inputfile STR	Path to the coordinates of the inputgraph
      -o STR, --outputdir STR	Path to the output directory.
	    -l INT, --l_value INT 	Maximum amount of edges between two nodes.
	    -k INT, --k_value INT	Maximum amount of neighobors of a node.

Inputfiles have the following structure:

    [number of node]:[coordinates] for every node in the graph
    [node],[node] for every edge in the graph

Example can be shown in [Lamman-Graph-steif](https://codebase.helmholtz.cloud/julienne.lehmann/fortgeschrittene-methoden-bioinf/-/blob/main/pebble_game/testgraphs/Laman-Graph-steif?ref_type=heads) which can be found in `cd pebble_game/testgraphs/`:

    1:0,-1,0
    2:4,2,1
    3:7,0,1
    4:9,3,2
    5:11,0,1
    6:14,2,1
    7:18,-1,0
    1,2
    1,3
    2,3
    2,4
    3,4
    4,5
    4,6
    5,6
    5,7
    6,7
    1,7



## 3. Protein Rigidity
Run this script to identify rigid components within a protein molecule given in a pdb file. Therefore start it like this:

    python protein_ridgity/protein_ridgity.py
 There you have the  following command line options as shown in the help page:
 
`python protein_ridgity/protein_ridgity.py -h`

    usage: protein_ridgity.py [-h] [-i INPUTFILE] [-o OUTPUTDIR] [-l L_VALUE] [-k K_VALUE]
    
    This script runs the generic Pebble Game for (k, l)-sparse protein graphs in 3D.
    optional arguments: 
      -h, --help            	show this help message and exit
      -i STR, --inputfile STR	Path to the coordinates of the inputgraph
      -o STR, --outputdir STR	Path to the output directory.
	  -l INT, --l_value INT 	Maximum amount of edges between two nodes.
	  -k INT, --k_value INT		Maximum amount of neighobors of a node.
    

Inputfiles have to be in form of a pdb file. A description of these can be found [here](https://pdb101.rcsb.org/learn/guide-to-understanding-pdb-data/introduction). Files can be downloaded from the [protein database](https://www.rcsb.org/).

## Contacts
 - Julienne Lehmann, [julienne.lehmann@online.de](julienne.lehmann@online.de)
 - Anne Hegewald, [anhe2001@gmail.com](anhe2001@gmail.com)

